output "internal_gateway" {
  value = "${var.chart_name}-loki-distributed-gateway.${var.namespace}.svc.cluster.local"
}
output "internal_front" {
  value = "${var.chart_name}-loki-distributed-query-frontend.${var.namespace}.svc.cluster.local"
}

output "service_url" {
  value = "http://${var.chart_name}-loki-distributed-query-frontend.${var.namespace}.svc.cluster.local:3100"
}
